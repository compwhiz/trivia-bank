<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/nova');
});

Route::get('install', function () {
    Artisan::call('migrate:fresh');
    Artisan::call('view:clear');
    Artisan::call('config:clear');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
